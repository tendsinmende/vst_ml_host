# VST ML Host

Host application for the project, used to:

- Generate sound data base via HELM-Vst
- Train Encoder / Decoder
- Generate Helm settings based on input sound


## Building and Running

1. If not already done, install [rust](rust.org) on your system.
2. If not already done, install python and the following modules libraries:
   - ML stuff, dont know yet.

change into this directory and execute.
```
cargo run --release
```
