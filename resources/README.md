# Resources
This folder contains binary resources used by the application

## Content
- helm: the [helm VST](https://tytel.org/helm/) licensed under GPL.
- surge: the [surge](https://surge-synthesizer.github.io/) vst.
