use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::OpenOptions;
use std::fs::read;
use std::fs::read_to_string;
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::io::{Error, Write};

use vst::api::MidiEvent;
use vst::host::HostBuffer;
use vst::host::{Host, PluginLoader, PluginInstance};
use vst::plugin::{PluginParameters, Plugin};

use serde::{Serialize, Deserialize};

use crate::training::ParameterSet;

pub const TRAIN_DIR: &str = "resources/train/";
///How many seconds long the generated wav files should be long.
pub const TRAIN_DATA_LEN: usize = 2;
///How many samples per second should be generated
pub const TRAIN_SAMPLE_RATE: usize = 44100;
///How many samples are generated in total per file.
pub const TRAIN_NUM_SAMPLES: usize = TRAIN_DATA_LEN * TRAIN_SAMPLE_RATE;


///A single training parameter with its possible values.
#[derive(Serialize, Deserialize, Debug)]
struct TrainParam{
    name: String,
    index: i32,
    values: Vec<f32>
}

///Training configuration used in gen_data to generate training data pairs of sound file and setting.
#[derive(Serialize, Deserialize, Debug)]
struct TrainConf{
    parameters: Vec<TrainParam>
}

impl TrainConf{
    pub fn save(&self){

	let final_string = serde_json::to_string_pretty(self).expect("Failed to convert conf to string");

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open("data_conf.json").unwrap();
        write!(file, "{}", final_string).unwrap();
    }

    pub fn load() -> Result<Self, Error>{
	let data = std::fs::read_to_string("data_conf.json")?;
	
	let s: Self = serde_json::from_str(&data).expect("Failed to parse json: ");
	Ok(s)
    }

    //Pops the next parameter and builds all buildable subsets recusivly
    fn build_set(mut params: Vec<TrainParam>) -> Vec<ParameterSet>{
	debug_assert!(params.len() > 0, "build_set(params): params vec was empty");
	
	//Recursion aboard
	if params.len() == 1{
	    let p = params.pop().unwrap();
	    let mut p_set = vec![];
	    for v in p.values{
		let mut params = HashMap::new();
		params.insert(p.index, (p.name.clone(), v));
		p_set.push(ParameterSet{
		    id: 0,
		    params,
		    path: "UNSET_PATH".to_string()
		})
	    }

	    return p_set;
	}
	
	let this_param = params.pop().unwrap();

	//Build all possible sub sets, there must be at least one left.
	let sub_sets = Self::build_set(params);

	let mut this_sets = Vec::new();
	//For each sub set, append this parameter in all its possible values

	for set in sub_sets.into_iter(){
	    for v in this_param.values.iter(){
		let mut set_clone = set.clone();
		if set_clone.params.insert(this_param.index, (this_param.name.clone(), v.clone())).is_some(){
		    println!("Warning: overwrote parameter");
		}
		this_sets.push(set_clone);
	    }
	}

	this_sets
    }
    
    pub fn to_parameter_sets(self) -> Vec<ParameterSet>{

	let num_parameter = self.parameters.iter().fold(1, |num, par| num*par.values.len());
	println!("Generating for {} parameter sets...", num_parameter);
	
	let sets = Self::build_set(self.parameters);

	assert!(sets.len() == num_parameter, "Num sets does not amount to the calculated size: sets={} != num_parameter={}", sets.len(), num_parameter);
	
	sets
    }
}

pub fn generate_default_train_conf(host: Arc<Mutex<crate::GenHost>>, instance: &mut PluginInstance){
    let pobj = instance.get_parameter_object();
    let mut conf = TrainConf{parameters: Vec::with_capacity(instance.get_info().parameters as usize)};
    
    for i in 0..instance.get_info().parameters{
	conf.parameters.push(TrainParam{
	    name: pobj.get_parameter_name(i),
	    index: i,
	    values: vec![pobj.get_parameter(i)]
	})
    }

    //Save default train conf
    conf.save();
}

fn prepare_working_directory(dir: &Path) -> Result<(), std::io::Error>{
    if dir.exists(){
	println!("    removing existing target directory!");
	std::fs::remove_dir_all(dir)?;
    }

    std::fs::create_dir(dir)?;

    Ok(())
}


fn midi_node() -> vst::api::MidiEvent{
    vst::api::MidiEvent{
        event_type: vst::api::EventType::Midi,
        byte_size: core::mem::size_of::<MidiEvent>() as i32,
        delta_frames: 0, //Start at start of this (big) processing block
        flags: vst::api::MidiEventFlags::REALTIME_EVENT.bits(),
        note_length: TRAIN_NUM_SAMPLES as i32,
        note_offset: 0,
        midi_data: [0b10010000, 0b01000101, 0b01111111], //b0: 1001(note-on)0000(channel 1), b1: (01000101: Node C3), b3: (01111111: Velocity max)
        _midi_reserved: 0,
        detune: 0,
        note_off_velocity: 0,
        _reserved1: 0,
        _reserved2: 0,
	
    }
}

///Takes some vst host. Then generates sound files by passing a 440Hz Sine to the VST while walking through every parameter combination in 10% steps.
pub fn gen_train_data(host: Arc<Mutex<crate::GenHost>>, instance: &mut PluginInstance){
    let train_path = Path::new(TRAIN_DIR);
    prepare_working_directory(&train_path).expect("Failed to initialize training directory");


    let pobj = instance.get_parameter_object();
    //Load the config
    let td_conf = TrainConf::load().unwrap();

    //now create each possible parameter set based on the supplied conf
    let mut sets = td_conf.to_parameter_sets();


    let num_in = instance.get_info().inputs;
    let num_out = instance.get_info().outputs;

    println!("Generating for num_inputs={}, num_outputs={}", num_in, num_out);
    
    let mut host_buffer: HostBuffer<f32> = HostBuffer::new(num_in as usize, num_out as usize); //Mono audio buffer;

    let input_buffer = vec![vec![0.0 as f32; TRAIN_NUM_SAMPLES]; num_in as usize];

    let mut midi_node = midi_node();
    let midi_ptr: *mut vst::api::MidiEvent = &mut midi_node as *mut _;
    let node_pointer: *mut vst::api::Event = unsafe{std::mem::transmute(midi_ptr)};
    //Now, for each set: Generate 
    for (idx, s) in sets.iter_mut().enumerate(){
	//Configure instance based on set
	s.configure_instance(instance);

	
	//prepare note to play and audio buffer to fill (2 x 44100 Hz for our wave file)
	let mut out_buffer = vec![vec![0.0 as f32; TRAIN_NUM_SAMPLES]; num_out as usize];
	//Bind empty in puffer and empty out buffer to host
	let mut audio_buffer = host_buffer.bind(&input_buffer,&mut out_buffer);
	
	//Send midi node and process
	instance.process_events(&vst::api::Events{
	    num_events: 1,
	    _reserved: 0,
	    events: [node_pointer; 2],
	    
	});

	//Generate audio file
	instance.process(&mut audio_buffer);
	//Save audio file and json to training data_location
	s.save(idx, &out_buffer);
    }
}
