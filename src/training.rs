use std::collections::HashMap;
use std::fs::OpenOptions;
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::io::{Error, Write};


use vst::buffer::AudioBuffer;
use vst::host::{Host, PluginLoader, PluginInstance};
use vst::plugin::{PluginParameters, Plugin};

use serde::{Serialize, Deserialize};

use crate::gen_data::TRAIN_DIR;


#[derive(Serialize, Deserialize, Clone)]
pub struct ParameterSet{
    ///Id of this data set
    pub id: usize,
    ///The parameters with which the wave file was generated
    pub params: HashMap<i32, (String, f32)>,
    ///Path to the wave file
    pub path: String,
}

impl ParameterSet{
    ///Configures `instance` based this parameter set.
    pub fn configure_instance(&self, inst: &mut PluginInstance){
	let obj = inst.get_parameter_object();
	for (index, (name, value)) in &self.params{
	    obj.set_parameter(*index, *value);
	}
    }

    pub fn save(&mut self, set_idx: usize, audio_buffers: &[Vec<f32>]){
	self.id = set_idx;
	let path = format!("{}train_{}.wav", TRAIN_DIR, set_idx);
	let json_path = format!("{}train_{}.json", TRAIN_DIR, set_idx);
	self.path = path;
	
	let spec = hound::WavSpec {
	    channels: audio_buffers.len() as u16,
	    sample_rate: crate::gen_data::TRAIN_SAMPLE_RATE as u32,
	    bits_per_sample: 32,
	    sample_format: hound::SampleFormat::Float,
	};

	let mut writer = hound::WavWriter::create(&self.path, spec).unwrap();

	//Collect channels into binary (TODO do not reallocate)
	let mut samples = vec![0.0; audio_buffers.len() * audio_buffers[0].len()];

	for (off, src_samples) in audio_buffers.iter().enumerate(){
	    let mut offset = off;
	    for s in src_samples.iter(){
		samples[offset] = *s;
		offset += audio_buffers.len();
	    }
	}
	//Write samples to file
	for s in samples{
	    writer.write_sample(s).expect("Failed to write sample");
	}

	//Now save self to json there as well
	let final_string = serde_json::to_string_pretty(self).expect("Failed to convert data set to string");

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(json_path).unwrap();
        write!(file, "{}", final_string).unwrap();
	println!("Saved: {}", self.path);
    }
}
