extern crate vst;
extern crate serde;
extern crate serde_json;
extern crate hound;
extern crate iced;

use std::path::Path;
use std::sync::{Arc, Mutex};

use vst::host::{Host, PluginLoader};
use vst::plugin::Plugin;
use vst::editor::Editor;

use iced::{Settings,Sandbox};

///Module used for generating the training data in `resources/train/`
pub mod gen_data;
use gen_data::*;

pub mod training;

pub mod ui;

pub struct GenHost;

impl Host for GenHost {
    fn automate(&self, index: i32, value: f32) {
        println!("Parameter {} had its value changed to {}", index, value);
    }
}

fn main() {    
    ui::Ui::run(Settings::default());
}





