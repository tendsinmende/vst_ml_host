//! Simple ui used to controll generating training data and interaction with finished model.

use std::path::Path;
use std::sync::Arc;
use std::sync::Mutex;

use iced::{button, Align, Button, Column, Element, Sandbox, Settings, Text};
use vst::host::PluginInstance;
use vst::host::PluginLoader;
use vst::plugin::Plugin;

use crate::GenHost;
use crate::gen_data;
use crate::gen_data::TRAIN_NUM_SAMPLES;
use crate::gen_data::TRAIN_SAMPLE_RATE;

#[derive(Debug, Clone, Copy)]
pub enum UiMsg{
    GenerateDefaultConfig,
    GenerateTrainData,
    TrainModell,
}

pub struct Ui{

    instance: PluginInstance,
    host: Arc<Mutex<GenHost>>,
    
    gen_default_button: button::State,
    gen_train_data_button: button::State,
    train_modell_button: button::State,
}

impl Drop for Ui{
    fn drop(&mut self){
	
	self.instance.stop_process();
	self.instance.suspend();
    }
}

impl Sandbox for Ui {
    type Message = UiMsg;

    fn new() -> Self {

	//Startup vst host
	
	let path = if cfg!(windows){
	    panic!("windows helm dll not included yet!");
	    #[allow(unreachable_code)]
	    "resources/helm/windows_x64/helm.dll"
	}else if cfg!(unix){
	    "resources/helm/linux_x64/helm.so"
	}else{
	    panic!("Running on unsupported system. Supported: [Linux x64, Windows x64]");
	};

	println!("Loading helm @ {}", path);

	// Create the host
	let host = Arc::new(Mutex::new(GenHost));
	let path = Path::new(path);
	// Load the plugin
	let mut loader = PluginLoader::load(path, host.clone())
	    .unwrap_or_else(|e| panic!("Failed to load plugin: {}", e));

	// Create an instance of the plugin
	let mut instance = loader.instance().unwrap();

	// Get the plugin information
	let info = instance.get_info();

	
	println!(
            "Loaded '{}':\n\t\
             Vendor: {}\n\t\
             Presets: {}\n\t\
             Parameters: {}\n\t\
             VST ID: {}\n\t\
             Version: {}\n\t\
             Initial Delay: {} samples",
            info.name, info.vendor, info.presets, info.parameters, info.unique_id, info.version, info.initial_delay
	);

	// Initialize the instance, then suspend until needed.
	instance.init();
	instance.suspend();
	//Setup buffer size and sampling. Currently this is hardcoded at 44100Hz @ 88200samples. So one process call should process the whole tone.
	instance.set_block_size(TRAIN_NUM_SAMPLES as i64);
	instance.set_sample_rate(TRAIN_SAMPLE_RATE as f32);
	//resume to processing state to generate train data.
	instance.resume();
	instance.start_process();
	
	Ui{
	    instance,
	    host,
	    
	    gen_default_button: Default::default(),
	    gen_train_data_button: Default::default(),
	    train_modell_button: Default::default(),
	}
    }

    fn title(&self) -> String {
        String::from("Counter - Iced")
    }

    fn update(&mut self, message: UiMsg) {
        match message {
            UiMsg::GenerateDefaultConfig => {
		gen_data::generate_default_train_conf(self.host.clone(), &mut self.instance);
	    },
	    UiMsg::GenerateTrainData => {
		gen_data::gen_train_data(self.host.clone(), &mut self.instance);
	    }
	    UiMsg::TrainModell => {
		println!("Training model not yet implemented");
	    }
        }
    }

    fn view(&mut self) -> Element<UiMsg> {
        Column::new()
            .padding(20)
            .align_items(Align::Center)
            .push(
                Button::new(&mut self.gen_default_button, Text::new("Generate Default Config"))
                    .on_press(UiMsg::GenerateDefaultConfig),
            )
            .push(Button::new(&mut self.gen_train_data_button, Text::new("Generate Training Data"))
                  .on_press(UiMsg::GenerateDefaultConfig),
            )
            .push(
                Button::new(&mut self.train_modell_button, Text::new("Train Model"))
                    .on_press(UiMsg::TrainModell),
            )
            .into()
    }
}
